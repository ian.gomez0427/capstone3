// Custom hook for cart handling
import { useContext, useEffect, useState } from 'react';
import UserContext from '../UserContext';

export function useCart() {
  const { user } = useContext(UserContext);
  const [cartItems, setCartItems] = useState([]);

  useEffect(() => {
    if (user) {
      console.log('User Object:', user);
      fetchCartItems(); // Fetch cart items here
    }
  }, [user]);

  const fetchCartItems = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/api/cart/view-cart`, {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      });
      const result = await response.json();
      console.log('API Response:', result); // Log the response
      if (result.data) {
        setCartItems(result.data.cartItems);
      }
    } catch (error) {
      console.error("Error fetching cart items:", error);
    }
  };

const updateCartItem = async (itemId, newQuantity) => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/api/cart/update-item`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
        body: JSON.stringify({ itemId, quantity: newQuantity }),
      });

      const result = await response.json();
      console.log('Update Item Response:', result); // Log the response

      if (result.data) {
        setCartItems(prevCartItems =>
          prevCartItems.map(item =>
            item._id === itemId ? { ...item, quantity: newQuantity } : item
          )
        );
      }
    } catch (error) {
      console.error('Error updating cart item:', error);
    }
  };
  // Other functions for adding/removing items and placing orders

  return {
    cartItems,
    fetchCartItems,
    updateCartItem,
    // ... other functions
  };
}
