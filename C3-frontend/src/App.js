import './App.css';
import AppNavbar from './components/AppNavbar.js';
import Home from './pages/Home.js';
import Collections from './pages/Collections.js';
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import NotFound from './pages/NotFound.js';
import Profile from './pages/Profile';
import AddCollection from './pages/AddCollection.js';
import CartPage from './components/CartPage.js';
import CollectionItem from './pages/CollectionItem';
import CollectionsPage from './components/CollectionsPage';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import {useState, useEffect} from 'react';
import { UserProvider } from './UserContext.js';


function App() {
  const [user, setUser] = useState({
    // token: localStorage.getItem ('token')
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/api/users/details`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        "Content-Type": "application/json"
      },
        body: JSON.stringify({
        id: localStorage.getItem('userId')
      })
    })
    .then(response => response.json())
    .then(result => {

      if(typeof result._id !== 'undefined'){
        // even if we refresh the browser, the 'user' state will still have its values re-assigned
        setUser({
          id: result._id,
          isAdmin: result.isAdmin
      })
    } else {
      setUser({
        id: null,
        isAdmin: null
      })
    }

  })
  }, [])

  return (
    <>
      <UserProvider value = {{user, setUser, unsetUser}}>
        <Router> 
          <AppNavbar/>
          <Container>
          <Routes>
            <Route path='/' element={<Home/>} />
            <Route path='/collections' element={<Collections/>} />
            <Route path='/collections/:collectionId' element={<CollectionItem/>} />
            <Route path="/collections/add" element={<AddCollection />} />
            <Route path='/register' element={<Register/>} />
            <Route path='/login' element={<Login/>} />
            <Route path='/logout' element={<Logout/>} />
            <Route path="/profile" element={<Profile />} />
            <Route path="/cart" element={<CartPage/>} />
            <Route path="*" element={<NotFound />} /> 
            <Route path="/collections/all" element={<CollectionsPage />} />
         

          </Routes>
          </Container>
        </Router>
      </UserProvider>
    </>
  );
}

export default App;
