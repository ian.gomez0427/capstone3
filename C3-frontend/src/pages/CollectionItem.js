// view collection details + add to cart

import { useState, useEffect, useContext} from 'react';
import { Container, Card, Button, Row, Col} from "react-bootstrap";
import {useParams, Link, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal from 'sweetalert2';

export default function CollectionItem(){
	const {collectionId} = useParams();
	const{user} = useContext(UserContext);
	const navigate = useNavigate();
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [imageUrl, setImageUrl] = useState("");


	const addToCart = () => {
    const token = localStorage.getItem('token');
    console.log('Stored Token:', token); // Log the token

    fetch(`${process.env.REACT_APP_API_URL}/api/cart/add-to-cart`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`, // Use the stored token
      },
      body: JSON.stringify({
        collectionId: collectionId,
      }),
    })

		.then(response => response.json())
		.then(result => {
			// console.log(result)
			if (result.message === 'Collection added to cart successfully'){
				Swal.fire({
		            title: 'One Piece Added to Cart',
		            text: 'View Cart to Checkout your One Piece',
		            icon: 'success'
		         })
				// to redirect back to the Collections page after adding to cart
				navigate('/collections')
			}else{
				Swal.fire({
		            title: 'Something went wrong',
		            text: 'Please try again',
		            icon: 'error'
		         })
			}
		})
	}


console.log('Received Image URL:', imageUrl);
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/api/collections/${collectionId}`)
		.then(response => response.json())
		.then(result => {
			setName(result.name)
			setDescription(result.description)
			setPrice(result.price)
			setImageUrl(result.imageUrl);
			})
	}, [collectionId])
console.log('Returned Image URL:', imageUrl);
	return(
		
		<Container 
			className = "mb-5" 
			style={{ border: 'none', fontWeight: 'bold', color: 'red', fontFamily: "'Caveat', cursive",}}
		>
			<Row>

				<Col >
					<Card style={{ border: 'none' }}>
						<img
			              src={`${process.env.REACT_APP_API_URL}/uploads/${imageUrl}`} // Construct the image URL
			              alt={name}
			              style={{ maxWidth: '100%', height: 'auto' }}
			              className="my-5"
			            />
			        </Card>
			    </Col>

			    <Col className="my-5">
					<Card style={{ border: 'none' }}>

				        <Card.Body className="d-flex flex-column align-items-center my-5">
				            <Card.Title><h1>{name}</h1></Card.Title>

				            <Card.Subtitle>Description</Card.Subtitle>
				            <Card.Text>{description}</Card.Text>

				            <Card.Subtitle>Price:</Card.Subtitle>
				            <Card.Text>{price}</Card.Text>
				        

				          {	(user.id !== null)?
				            <>
				            	<div className="text-center">
				            	<h3>Aye! Aye! Pirate!</h3>
				            	<h2>You found the One Piece!</h2>
				            	</div>
				            	<Button 
				            		variant="primary" 
				            		onClick = {() => addToCart(collectionId)}
				            		className = "btn btn-primary my-3 btn-lg"
				            	>
				            		Add to Cart
				            	</Button>
				            </>
				            :
				            <>
				            <card className="d-flex flex-column align-items-center">
				            	<div className="text-center">
				            	<h3>Do you want to add this One Piece Collection to Cart?</h3>
				            	<h2>You'll have to be a pirate member!</h2>
				            	</div>
				            	<Link className = "btn btn-danger btn-black my-3 btn-lg" style={{ width: '50%' }} to = '/login'> Login </Link>
				            	<Link className = "btn btn-danger btn-black btn-lg" style={{ width: '50%' }} to = '/register'> Register </Link>
				            
				            </card>
				            </>
				            }
				        </Card.Body>
				        
			        </Card>

			    </Col>

			</Row>

		</Container>
	)
}