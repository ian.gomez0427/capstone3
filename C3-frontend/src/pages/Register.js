import { Form, Button, Container, Row, Col } from "react-bootstrap";
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext.js';
import {Navigate, useNavigate  } from 'react-router-dom';
import Swal from 'sweetalert2';
import Image from 'react-bootstrap/Image';

export default function Register(){
	const navigate = useNavigate ();
	const { user } = useContext(UserContext);
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");
	const [isActive, setIsActive] = useState(false);


	function registerUser(event) {
		//Prevents page load upon form submission
		event.preventDefault();

		//sends a request to the /register endpoint which will include all the fields necessary for that route.
		fetch (`${process.env.REACT_APP_API_URL}/api/users/register`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password
			})
		}).then(response => response.json())
			.then(result => {
				if(result) {
					// setUser({ registered: true });
					setFirstName("")
					setLastName("")
					setEmail("")
					setMobileNo("")
					setPassword("")
					setConfirmPassword("")
					
					// alert (result.message)
					Swal.fire({
			            title: 'You have become a pirate!',
			            text: 'Enjoy One Piece Collections.',
			            icon: 'success'
			         });

						navigate('/profile');
				} else {
					// alert ('Please try again')
					Swal.fire({
			            title: 'Something went wrong',
			            text: 'Please try again.',
			            icon: 'error'
			          })
				}
		}) 
	}

	useEffect(() => {
		// Insert effect here
		if (
			firstName !== "" && 
			lastName !== "" && 
			email !== "" && 
			mobileNo !== "" && 
			password !== "" && 
			confirmPassword !== "" && 
			password === confirmPassword && 
			mobileNo.length === 11
			) 
		{
			setIsActive(true)
		} else {
			setIsActive(false)
		}

	}, 
	[firstName, lastName, email, mobileNo, password, confirmPassword]);

	return (
		// (user.token !== null)?
		(user.id !== null)?
			<Navigate to = '/' />
		:
		<Container className = "mt-3">
	    	<Row className="justify-content-center">
	      		<Col md={8} lg={6} xl={5}>
			
					<Form onSubmit = {(event) => registerUser(event)} className="login-form">
						<div className="background-overlay"></div>
					        <h1 className="my-5 text-center">
					            <Image
					                src={require("../images/registernobg.png")}
					                style={{ width: '200px', height: 'auto' }}
					            />
					        </h1>
				            <Form.Group>
				                <Form.Label >First Name:</Form.Label>
				                <Form.Control 
					                type="text" 
					                placeholder="Enter First Name" 
					                required
					                value = {firstName}
					                onChange = {event => {setFirstName(event.target.value)}}
				                />
				            </Form.Group>
				            <Form.Group>
				                <Form.Label className="mt-3">Last Name:</Form.Label>
				                <Form.Control 
					                type="text" 
					                placeholder="Enter Last Name" 
					                required
					                value = {lastName}
						            onChange = {event => {setLastName(event.target.value)}}
					            />
				            </Form.Group>
				            <Form.Group>
				                <Form.Label className="mt-3">Email:</Form.Label>
				                <Form.Control 
					                type="email" 
					                placeholder="Enter Email" 
					                required
					                value = {email}
						            onChange = {event => {setEmail(event.target.value)}}		
				                />
				            </Form.Group>
				            <Form.Group>
				                <Form.Label className="mt-3">Mobile No:</Form.Label>
				                <Form.Control 
					                type="number" 
					                placeholder="Enter 11 Digit No." 
					                required
					                value = {mobileNo}
						            onChange = {event => {setMobileNo(event.target.value)}}	
				                />
				            </Form.Group>
				            <Form.Group>
				                <Form.Label className="mt-3">Password:</Form.Label>
				                <Form.Control 
					                type="password" 
					                placeholder="Enter Password" 
					                required
					                value = {password}
						            onChange = {event => {setPassword(event.target.value)}}
				                />
				            </Form.Group>
				            <Form.Group>
				                <Form.Label className="mt-3">Confirm Password:</Form.Label>
				                <Form.Control 
					                type="password" 
					                placeholder="Confirm Password" 
					                required
					                value = {confirmPassword}
						            onChange = {event => {setConfirmPassword(event.target.value)}}
				                />
	            

	            <Button
		            className="my-5"
		            type="submit"
		            disabled={isActive === false}
		            style={{ width: '100px'}} 
		          > 
		          <Image
		                src={require("../images/registernobg.png")}
		                style={{ maxWidth: '100%', maxHeight: '100%'}}
		            />
		          </Button>
		        </Form.Group>
                    
        </Form>
       
        </Col>
      </Row>
      </Container>
		)
}
