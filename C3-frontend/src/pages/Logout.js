import { Navigate } from "react-router-dom";
import UserContext from '../UserContext.js';
import {useContext, useEffect} from 'react';

export default function Logout() {
	const {unsetUser, setUser} = useContext (UserContext);

	// clears the Localstorage/ token
	unsetUser();
	localStorage.removeItem('token');


	//clears the token from the global user state
	useEffect(() => {
		// setUser({token: null});
		setUser({
			id: null,
			isAdmin: null,
			token: null
		});
	}, [setUser]);

	return (
		<Navigate to='/login' />
	)

}

