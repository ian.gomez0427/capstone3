// not in use

// import collectionsData from '../data/collectionsData.js';
// import CollectionCard from '../components/CollectionCard.js';
import {useEffect, useState, useContext} from 'react';
import AdminView from '../components/AdminView.js';
import UserView from '../components/UserView.js';
import UserContext from '../UserContext.js'

// displaying collections by loop
export default function Collections () {
	const {user} = useContext (UserContext);
	const [collections, setCollections] = useState([]);

	const fetchCollections = () =>{
		fetch(`${process.env.REACT_APP_API_URL}/api/collections/all`)
		.then(response => response.json())
		.then(result => {
			setCollections(result)
		})
	}
	// the useEffect hook will run everytime the Collections page loads, which will then retrieve all collections from the API and set them to their specific CollectionCards
	useEffect(() => {
	fetchCollections()
	}, [])

	return(
		<>
			{
				(user.isAdmin === true)?
					<AdminView collectionsData = {collections} fetchCollections = {fetchCollections}/>
				:
					<UserView collectionsData = {collections}/>
			}
		</>
	)
}

