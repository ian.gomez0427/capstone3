// admin view - add collection

import React, {useState, useContext} from 'react';
import { Form, Button } from "react-bootstrap";
import {Navigate, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext.js';

export default function AddCollection(){
	// Initialization 
	const navigate = useNavigate();
	const {user} = useContext (UserContext);
	const [image, setImage] = useState(null);

	// States
	const [category, setCategory] = useState("")
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0)

	// Functions
	function handleImageUpload(event) {
	    const selectedImage = event.target.files[0];
	    setImage(selectedImage);
	}

	function createCollection (event) {
		// prevents the default behavior of page reload when submitting a form
		event.preventDefault();

		let token = localStorage.getItem ('token');

		 const formData = new FormData();
		 	formData.append('category', category);
		    formData.append('name', name);
		    formData.append('description', description);
		    formData.append('price', price);
		    formData.append('image', image);

		// Fetch function to the collection creation API
		fetch (`${process.env.REACT_APP_API_URL}/api/collections/`, {
			method: 'POST',
			headers: {
				// 'Content-Type': 'application/json',
				Authorization: `Bearer ${token}`
			},
			// body: JSON.stringify({
			// 	name: name,
			// 	description: description,
			// 	price: price
		// })
		body: formData
	})
		.then(response => response.json())
			.then(result => {
				if(result) {
					Swal.fire({
			            title: 'New Collection Added',
			            icon: 'success'
			        })

					// to clear the form after submission
			        setName('')
			        setDescription('')
			        setPrice(0)
			        setImage(null);

			        // redirect to /collections after creating a new collection
			        navigate('/collections')

			    } else {
			    	Swal.fire({
			            title: 'Something went wrong',
			            text: 'Collection creation unsuccessful',
			            icon: 'error'
			          })
			    }
		})
}

	return (
		(user.isAdmin === true)?
			<>
			 	<h1 className="my-5 text-center">Add Collection</h1>
                <Form onSubmit={event => createCollection(event)}>
                	<Form.Group>
					    <Form.Label>Category:</Form.Label>
					    <Form.Control as="select" value={category} onChange={event => setCategory(event.target.value)} required>
					        <option value="">Choose the Category</option>
					        <option value="Poster">Poster</option>
					        <option value="Figure">Figure</option>
					        <option value="Manga">Manga</option>
					        {/* Add more categories as needed */}
					    </Form.Control>
					</Form.Group>
                    <Form.Group>
                        <Form.Label>Name:</Form.Label>
                        <Form.Control type="text" placeholder="Enter Collection Name" required value={name} onChange={event => {setName(event.target.value)}}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Description:</Form.Label>
                        <Form.Control type="text" placeholder="Enter Description" required value={description} onChange={event => {setDescription(event.target.value)}}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Price:</Form.Label>
                        <Form.Control type="number" placeholder="Enter Price" required value={price} onChange={event => {setPrice(event.target.value)}}/>
                    </Form.Group>
                    <Form.Group>
				      <Form.Label>Image:</Form.Label>
				      <Form.Control type="file" accept="image/*" onChange={event => handleImageUpload(event)} />
				      {/* Display the selected image */}
				      {image && <img src={URL.createObjectURL(image)} alt="Selected" style={{ maxWidth: '100px' }} />}
				    </Form.Group>
                    <Button variant="primary" type="submit" className="my-5">Submit</Button>
                </Form>
			</>
		:
			<Navigate to = '/collections' />
	)
}