import { Form, Button, Container, Row, Col } from "react-bootstrap";
import { useState, useEffect, useContext} from "react";
import UserContext from '../UserContext.js';
import {Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import Image from 'react-bootstrap/Image';


export default function Login() {
  // We're able to access the 'user' state from App.js through the use of react context/provider
  const {user, setUser} = useContext(UserContext);

  // State hooks to store the value of the input fields
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  // State to determine whether submit button is enabled or not
  const [isActive, setIsActive] = useState(false);

  function loginUser(event) {
    // 
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/api/users/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        console.log(result);

        if (result.accessToken) {
          localStorage.setItem ('token', result.accessToken);
          localStorage.setItem ('userId', result.userId);

          retrieveUserDetails(result.accessToken, result.userId)

          setEmail("");
          setPassword("");

          Swal.fire({
            title: 'Welcome Back Pirate!',
            text: 'Enjoy One Piece Collections.',
            icon: 'success'
          })

        } else {

          Swal.fire({
            title: 'Something went wrong',
            text: `${email} does not exist.`,
            icon: 'warning'
          })

        }
      })
  }

  const retrieveUserDetails = (token, userId) => {
    fetch(`${process.env.REACT_APP_API_URL}/api/users/details`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        id: userId
      })
    })
    .then(response => response.json())
    .then(result => {
      // once it gets the user details, we will set the global user state to have the ID and isAdmin properties of the user who is logged in.
      setUser({
        id: result._id,
        isAdmin: result.isAdmin
      })
    })
  }

  useEffect(() => {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return (
    // (user.token !== null) ?
    (user.id !== null) ?
      <Navigate to = '/profile' />
    :
    <Container className = "mt-3">
    <Row className="justify-content-center">
      <Col md={8} lg={6} xl={5}>
        
      <Form onSubmit={(event) => loginUser(event)} className="login-form">  
      <div className="background-overlay"></div>
        <h1 className="my-5 text-center">
            <Image
                src={require("../images/loginnobg.png")}
                style={{ width: '150px', height: 'auto' }}
            />
        </h1>
        <Form.Group 
          style={{
          position: 'relative', // Set the position to relative
        }}
        >
          <Form.Label>Email Address:</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter Email"
            required
            value={email}
            onChange={(event) => {
            setEmail(event.target.value);
            }}
          />
        </Form.Group>

        <Form.Group>
          <Form.Label className="mt-3">Password:</Form.Label>
          <Form.Control
            type="password"
            placeholder="Enter Password"
            required
            value={password}
            onChange={(event) => {
              setPassword(event.target.value);
            }}
          />

          <Button
            className="my-5"
            type="submit"
            disabled={isActive === false}
            style={{ width: '100px'}} 
          > 
          <Image
                src={require("../images/loginnobg.png")}
                style={{ maxWidth: '100%', maxHeight: '100%'}}
            />
          </Button>
        </Form.Group>
        
      </Form>

      </Col>
      </Row>
      </Container>

  )
}
