import React, { useContext, useState, useEffect } from 'react';
import { UserContext } from '../UserContext';
import { Container, Card} from 'react-bootstrap';
import { Navigate } from 'react-router-dom';

export default function Profile() {
  const { user, setUser } = useContext(UserContext);

  const [details, setDetails] = useState({});

  useEffect(() => {
    if (user.id) {
      fetch(`${process.env.REACT_APP_API_URL}/api/users/details`, {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          id: user.id,
        }),
      })
        .then(response => response.json())
        .then(result => {
          if (typeof result._id !== 'undefined') {
            setDetails(result);
            setUser(prevUser => ({ ...prevUser, firstName: result.firstName, lastName: result.lastName }));
          }
        });
    }
  }, [user.id, setUser]);

  return user.id === null ? (
    <Navigate to="/login" />
  ) 
  : 
  (
  <Container className="p-3 d-flex justify-content-center">
      <Card
        style={{
          backgroundImage: `url(${require('../images/wanted1.png')})`,
          backgroundSize: '100% 100%',
          backgroundPosition: 'center',
          backgroundRepeat: 'no-repeat',
          textAlign: 'center',
          width: '60vh', 
          height: '80vh', 
          maxWidth: '80vw',
          maxHeight: '80vh',
          position: 'relative', // Use relative positioning
          padding: 0, // Remove default padding
          overflow: 'hidden', // Hide overflow content


        }}
      >

        <div
          style={{
            position: 'absolute',
            bottom: 0,
            left: 0,
            right: 0,
            display: 'flex',       // Use flex display
            flexDirection: 'column', // Arrange children in a column
            justifyContent: 'flex-end', // Align items at the bottom
            height: '100%', // Occupy full height of the card
            padding: '5%', // Use percentage for padding
            boxSizing: 'border-box', // Include padding in dimensions
          }}
          >
          <h1 style={{ fontSize: '6vh', marginBottom: 1  }}>
            {`${details.firstName} ${details.lastName}`}
          </h1>
          <ul style={{ listStyle: 'none', fontSize: '2.5vh', padding: 0, marginBottom: 50 }}>
            <li>Email: {details.email}</li>
            <li>Mobile: {details.mobileNo}</li>
          </ul>
        </div>

      </Card>
  </Container>

  );
}


