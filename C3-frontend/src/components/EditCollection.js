import {Button, Modal, Form} from 'react-bootstrap';
import {useState} from 'react';
import Swal from 'sweetalert2';


export default function EditCollection({collection_id, fetchCollections}) {
	// Form states
	const [collectionId, setCollectionId] = useState('');
	const [category, setCategory] = useState('');
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [editImage, setEditImage] = useState(null);
	const [imageUrl, setImageUrl] = useState('');

	// for modal activation
	const [showEditModal, setShowEditModal] = useState(false);

	const openEditModal = (collectionId) => {
		fetch (`${process.env.REACT_APP_API_URL}/api/collections/${collectionId}`)
		.then(response => response.json())
		.then(result => {
			// pre-populate the form input fields with data from the API
			setCollectionId(result._id);
			setCategory(result.category);
	        setName(result.name);
	        setDescription(result.description);
	        setPrice(result.price);
	        setImageUrl(result.imageUrl);
		})

		// then, open the modal
		setShowEditModal(true);
	}

	const closeEditModal = () => {
        setShowEditModal(false);
        // setName('')
        // setDescription('')
        // setPrice('')
    };

    function handleEditImageUpload(event) {
	    const selectedImage = event.target.files[0];
	    setEditImage(selectedImage);
	  }

    const editCollection = (event, collectionId) => {
        event.preventDefault();

        const formData = new FormData();
        	formData.append('category', category);
		    formData.append('name', name);
		    formData.append('description', description);
		    formData.append('price', price);
		    if (editImage) {
		      formData.append('image', editImage);
		 }

        // Send the updated data to the API using fetch
        fetch(`${process.env.REACT_APP_API_URL}/api/collections/${collectionId}`, {
            method: 'PUT',
            headers: {
                // 'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: formData
        })
            .then(response => response.json())
            .then(result => {
            	if (result){
            		Swal.fire ({
            			title: 'Collection Updated!',
            			text: 'Collection successfully updated.',
            			icon: 'success'
            		})
            		// Triggers the fetching of all Collections after updating a Collection. The 'fetchCollections' function comes from the props that were passed from the Collections.js component to the AdminView.js component and finally to this EditCollection.js
            		fetchCollections();
            		closeEditModal();

            	} else {
            		Swal.fire ({
            		title: 'Something went wrong',
            		text: 'Please try again.',
            		icon: 'error'
            		})
            		fetchCollections();
            		closeEditModal();
            	}
            })
    }

	return(
		<>
			<Button 
				variant='primary' 
				size='sm' 
				style={{ width: '70px' }} 
				onClick={() => openEditModal(collection_id)}>Edit
			</Button>

			{/*Edit Modal*/}
			<Modal show= {showEditModal} onHide={closeEditModal}>
				<Form onSubmit = {event => editCollection(event, collectionId)}>
					<Modal.Header closeButton>
						<Modal.Title>Edit Collection</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						
						<Form.Group controlId="collectionCategory">
						    <Form.Label>Category</Form.Label>
						    <Form.Control
						        as="select"
						        value={category}
						        onChange={event => setCategory(event.target.value)}
						        required
						    >
						        <option value="">Choose the Category</option>
						        <option value="Poster">Poster</option>
						        <option value="Figure">Figure</option>
						        <option value="Manga">Manga</option>
						        {/* Add more categories as needed */}
						    </Form.Control>
						</Form.Group>

						<Form.Group controlId="collectionName">
                            <Form.Label>Name</Form.Label>
                            <Form.Control 
                            	type="text" 
                            	value={name} 
                            	onChange={event => setName(event.target.value)} 
                            	required
                            />
                        </Form.Group>
                        <Form.Group controlId="collectionDescription">
                            <Form.Label>Description</Form.Label>
                            <Form.Control 
                            	type="text" 
                            	value={description} 
                            	onChange={event => setDescription(event.target.value)}
                            	required
                            />
                        </Form.Group>
                        <Form.Group controlId="collectionPrice">
                            <Form.Label>Price</Form.Label>
                            <Form.Control 
                            	type="number" 
                            	value={price} 
                            	onChange={event => setPrice(event.target.value)} 
                            	required
                            />
                        </Form.Group>

                        {/* Display the existing image */}
			            <div className="my-3">
			              <img
			                src={`${process.env.REACT_APP_API_URL}/uploads/${imageUrl}`}
			                alt={name}
			                style={{ maxWidth: '100px', maxHeight: '100px' }}
			              />
			            </div>
			            {/* Input for editing image */}
			            <Form.Group controlId="editCollectionImage">
			              <Form.Label>Edit Image:</Form.Label>
			              <Form.Control type="file" accept="image/*" onChange={handleEditImageUpload} />
			            </Form.Group>

                        
					</Modal.Body>
					<Modal.Footer>
						<Button variant = 'secondary' onClick={closeEditModal}>Close</Button>
						<Button variant = 'success' type='submit'>Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>
		</>
		)
}
