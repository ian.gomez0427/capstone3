import { Row, Col, Card, Container } from 'react-bootstrap';
import Image from 'react-bootstrap/Image';
import {Link} from 'react-router-dom';

const responsiveImageStyle = {
    maxWidth: '100%',
    height: 'auto',
};

const darkBackgroundStyle = {
    position: 'relative',
    // padding: '2rem',
    color: 'white',
    fontFamily: "'Caveat'",
    background: 'linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(' + require("../images/banner.jpg") + ')',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
};

const collectButtonStyle = {
    fontSize: '24px', // Adjust the font size as needed
    color: 'white', // Hide text content
    fontFamily: "'Caveat'",
};


export default function Banner() {
    

    const keyframes = `
    @keyframes moveUp {
      0% {
        transform: translateY(100%);
      }
      100% {
        transform: translateY(-100%);
      }
    }
  `;

    return (
    	<>
        <Container>
        <Row className="align-items-center p-2 my-5" style={darkBackgroundStyle}>
            <Col sm={12} md={6} className="p-3 text-center" >
            
                <h1>
                    <Image
                        src={require("../images/opcollectionslogocolored.png")}
                        style={responsiveImageStyle}
                    />

                </h1>
                <Card style={{color: 'white', background: 'transparent', height: '450px', position: 'relative', overflow: 'hidden', display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
                  <div style={{ position: 'absolute', bottom: 0, width: '100%', height: '100%', animation: 'moveUp 20s linear infinite'}}>

                    <style>{keyframes}</style>
                    <p className="my-3" style={{ padding: '20px', fontSize: '20px'}}>
                      Wealth! Fame! Power! <br /><br />
                      The man who had everything in this world... <br />
                      The Pirate King, Gold Roger. <br /><br />
                      The great treasure he left behind, One Piece has opened the curtain on a grand era! It is a time when eager pirates set sail, battle, and become great! <br /><br />
                      The Great Age of Pirates! 
                      <br /><br />Words he spoke drove countless men out to sea. And so men set sights on the Grand Line, in pursuit of their dreams. The world has truly entered a Great Pirate Era!<br /><br />
                      Are you ready to set sail and find the One Piece?!
                    </p>

                  </div>
                </Card>
            </Col>
            <Col sm={12} md={6} className="p-0 my-5">
                <Image src={require("../images/opteam.png")} fluid alt="one piece"/>
            </Col>
        </Row>
        <Row>
            <Col className="text-center mb-5">
                <Link style={collectButtonStyle} className="btn btn-primary" to="/collections">Find One Piece Now!</Link>
            </Col>
        </Row>
        </Container>
        </>

    );
}
