import {Button,} from 'react-bootstrap';
import {useState} from 'react';
import Swal from 'sweetalert2';

export default function ArchiveCollection({ collection_id, fetchCollections, isActive }) {

	const archiveCollection = (collectionId) => {
		fetch(`${process.env.REACT_APP_API_URL}/api/collections/${collectionId}/archive`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
            .then(result => {
            	if (result){
            		Swal.fire ({
            			title: 'Collection Archived!',
            			text: 'Collection has been archived successfully.',
            			icon: 'success'
            		})
            		fetchCollections();

            	} else {
            		Swal.fire ({
            		title: 'Something went wrong',
            		text: 'Please try again.',
            		icon: 'error'
            		})
            		fetchCollections();
              	}
            })
	}

	const activateCollection = (collectionId) => {
		fetch(`${process.env.REACT_APP_API_URL}/api/collections/${collectionId}/activate`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
        .then(result => {
            if (result) {
                Swal.fire({
                    title: 'Collection Activated!',
                    text: 'Collection has been successfully activated.',
                    icon: 'success'
                });
                fetchCollections();

            } else {
                Swal.fire({
                    title: 'Something went wrong',
                    text: 'Please try again.',
                    icon: 'error'
                });
            }
        });
	}

	return(
		<>
			{isActive ?
				<Button variant='warning' size='sm' style={{ width: '70px' }} onClick={() => archiveCollection(collection_id)}>Archive</Button>
			:
				<Button variant='success' size='sm' style={{ width: '70px' }} onClick={() => activateCollection(collection_id)}>Activate</Button>
			}
		</>
	)
}
