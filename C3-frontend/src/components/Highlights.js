import { Card, Row, Col, Carousel } from 'react-bootstrap';
import { Link } from 'react-router-dom'; // Import the Link component
import poster1 from '../images/Collections/posters/postluffy.jpg';
import poster2 from '../images/Collections/posters/postlaw.jpg';
import poster3 from '../images/Collections/posters/postshanks.jpg';
import figure1 from '../images/Collections/figures/luffy.jpeg';
import figure2 from '../images/Collections/figures/zoro.jpg';
import figure3 from '../images/Collections/figures/shanks.jpeg';
import manga1 from '../images/Collections/manga/set1.jpg';
import manga2 from '../images/Collections/manga/set2.jpg';
import manga3 from '../images/Collections/manga/set3.jpg';

const imageStyle = {
    width: '100%', // Set the desired width
    height: 'auto', // Set the desired height
    objectFit: 'cover', // Maintain aspect ratio and cover the entire space
};

export default function Highlights() {
    const posters = [
        { image: poster1, alt: 'Slide 1', caption: '' },
        { image: poster2, alt: 'Slide 2', caption: '' },
        { image: poster3, alt: 'Slide 3', caption: '' },
    ];

    const figures = [
        { image: figure1, alt: 'Slide 1', caption: '' },
        { image: figure2, alt: 'Slide 2', caption: '' },
        { image: figure3, alt: 'Slide 3', caption: '' },
    ];

    const manga = [
        { image: manga1, alt: 'Slide 1', caption: '' },
        { image: manga2, alt: 'Slide 2', caption: '' },
        { image: manga3, alt: 'Slide 3', caption: '' },
    ];

    return (
        <Row className="my-5, text-center" >
            <Col xs={12} md={4} >
                <Card className = "cardHeight">
                    <Card.Body className="cardHighlight">
                        <Card.Title>
                            <Link to="/collections">
                                <h2>Posters</h2>
                            </Link>
                        </Card.Title>
                        <Carousel>
                            {posters.map((slide, index) => (
                                <Carousel.Item key={index}>
                                    <img
                                        className="d-block w-100"
                                        src={slide.image}
                                        alt={slide.alt}
                                        style={imageStyle}
                                    />
                                    <Carousel.Caption>
                                        <p>{slide.caption}</p>
                                    </Carousel.Caption>
                                </Carousel.Item>
                            ))}
                        </Carousel>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHeight">
                    <Card.Body className="cardHighlight">
                        <Card.Title >
                            <Link to="/collections">
                                <h2>Figures</h2>
                            </Link>
                        </Card.Title>
                        <Carousel>
                            {figures.map((slide, index) => (
                                <Carousel.Item key={index}>
                                    <img
                                        className="d-block w-100"
                                        src={slide.image}
                                        alt={slide.alt}
                                        style={imageStyle}
                                    />
                                    <Carousel.Caption>
                                        <p>{slide.caption}</p>
                                    </Carousel.Caption>
                                </Carousel.Item>
                            ))}
                        </Carousel>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHeight">
                    <Card.Body className="cardHighlight">
                        <Card.Title>
                            <Link to="/collections">
                                <h2>Manga</h2>
                            </Link>
                        </Card.Title>
                        <Carousel>
                            {manga.map((slide, index) => (
                                <Carousel.Item key={index}>
                                    <img
                                        className="d-block w-100"
                                        src={slide.image}
                                        alt={slide.alt}
                                        style={imageStyle}
                                    />
                                    <Carousel.Caption>
                                        <p>{slide.caption}</p>
                                    </Carousel.Caption>
                                </Carousel.Item>
                            ))}
                        </Carousel>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    );
}
