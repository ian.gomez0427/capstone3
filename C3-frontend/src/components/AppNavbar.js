import {Navbar, Nav, Container, NavDropdown } from 'react-bootstrap';
import {Link, NavLink} from 'react-router-dom';
import {useContext} from 'react';
import UserContext from '../UserContext.js'
import Image from 'react-bootstrap/Image';
import { FaHome } from 'react-icons/fa';
import { FaShip } from 'react-icons/fa';
import { FaShoppingCart } from 'react-icons/fa';
import { FaUser } from 'react-icons/fa';
import { FaSkullCrossbones } from 'react-icons/fa';
import { FaSignOutAlt } from 'react-icons/fa';
import { FaPlus } from 'react-icons/fa';
import { FaUserPlus } from 'react-icons/fa';

const NavMenuStyle = {
        color: 'red',
        fontFamily: "'Caveat",
    };

const navbarStyle = {
    position: 'sticky', 
	top: 0, // Position at the top
	left: 0, // Position at the left
	right: 0, // Position at the right
	zIndex: 1000, // Higher z-index to make sure the navbar is on top of other content
	backgroundImage: `url(${require('../images/navbg.png')})`,
	backgroundSize: 'cover',
	backgroundRepeat: 'no-repeat',
	backgroundAttachment: 'fixed',
	backgroundPosition: 'center top',
	fontSize: '20px',
	width: '100%', // Make the navbar span the full width

};


export default function AppNavbar() {
	const {user} = useContext (UserContext);
	const welcomeText = user.id !== null && user.lastName !== undefined
    ? user.isAdmin
        ? `Welcome, Admin ${user.firstName} ${user.lastName}`
        : `Welcome, ${user.firstName} ${user.lastName}`
    : "Welcome One Piece Collector!";

	return (
		<Navbar style={navbarStyle} expand="lg">
			<Container fluid>
			<Navbar.Brand as={Link} to='/'>
				<Image src={require("../images/collectionslogowhite.png")} width="300"/>
			</Navbar.Brand>
			<Navbar.Text style={{color: 'white'}}>{welcomeText}</Navbar.Text>
		    <Navbar.Toggle aria-controls = "basic-navbar-nav"/>
		    <Navbar.Collapse id="basic-navbar-nav">

		    	<Nav className = "ms-auto" >
		    		<Nav.Link as={NavLink} to="/" style={NavMenuStyle}>
				        <FaHome /> Home
				    </Nav.Link>
			    	<Nav.Link as={NavLink} to="/collections" style={NavMenuStyle}>
					    <FaShip /> Collections
					</Nav.Link>

		    		{ (user.id !== null) ?
			    		user.isAdmin ?
		    			<>
		    				<Nav.Link as={NavLink} to='/collections/add' style={NavMenuStyle}><FaPlus />Add Collection</Nav.Link>
		    				<Nav.Link as={NavLink} to='/logout' style={NavMenuStyle}> <FaSignOutAlt />Logout</Nav.Link>
			    		</>
				    	:
			    		<>
				    		<Nav.Link as={NavLink} to="/profile" style={NavMenuStyle}>
							    <FaSkullCrossbones /> Profile
							</Nav.Link>
		    				<Nav.Link as={NavLink} to="/cart" style={NavMenuStyle}>
							    <FaShoppingCart /> Cart
							</Nav.Link>
		    				<Nav.Link as={NavLink} to="/logout" style={NavMenuStyle}>
							    <FaSignOutAlt /> Logout
							</Nav.Link>
		    				
			    		</>
			    		:	
		    			<>
				    		<NavDropdown title={<span style={NavMenuStyle}><FaUserPlus />Be a Member</span>} id="basic-nav-dropdown">
				                <NavDropdown.Item as={NavLink} to="/register" >Register</NavDropdown.Item>
				                <NavDropdown.Item as={NavLink} to="/login" >Login</NavDropdown.Item>
				            </NavDropdown>
		    			</>		
			    	}

		    	</Nav>
		    	
		    </Navbar.Collapse>
		    
		    </Container>
		</Navbar>

		)
}