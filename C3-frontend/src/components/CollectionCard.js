import React from 'react';
import { Card, Row, Container} from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default function CollectionCard({ collection }) {
  const { _id, name, description, price, imageUrl } = collection; // Assuming there's an 'imageUrl' property for images

// style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', padding: '0 4%', height: '400px' }}

  return (

        <Card>
            <Container  style={{padding: '0 4%'}}>

            <Row style={{height: '400px', marginTop: "10px"}}>
              <img
                src={`${process.env.REACT_APP_API_URL}/uploads/${imageUrl}`}
                alt={collection.name}
                style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '400px' }}
              />
              </Row>

              <Row style={{height: '120px'}}>

            <Card.Body style={{ padding: '0 10%' }}>
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description: </Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price: {price}</Card.Subtitle>
            </Card.Body>

            </Row>

            </Container>

            <Link className="btn btn-primary" to={`/collections/${_id}`}>View Details</Link>

        </Card>
  );
}


CollectionCard.propTypes = {
  collection: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    imageUrl: PropTypes.string.isRequired // Add this line for the image URL
  })
}
