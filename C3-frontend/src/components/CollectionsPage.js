import React, { useState, useEffect } from 'react';
import { useLocation } from 'react-router-dom'; 
import CollectionCard from './CollectionCard';

export default function CollectionsPage() {
  const [collections, setCollections] = useState([]);
  const location = useLocation();

  useEffect(() => {
    // Fetch collections data from your API here
    async function fetchCollections() {
      try {
        const apiUrl = `${process.env.REACT_APP_API_URL}/collections`;
        const response = await fetch(apiUrl);

        if (!response.ok) {
          throw new Error('Network response was not ok');
        }

        const data = await response.json();
        setCollections(data);
      } catch (error) {
        console.error('Error fetching collections:', error);
      }
    }

    fetchCollections();
  }, []);

	const queryParams = new URLSearchParams(location.search);
  	const selectedCategory = queryParams.get('category');

  	const filteredCollections = selectedCategory
    ? collections.filter(collection => collection.category === selectedCategory)
    : collections;

  return (
    <div className="container">
      <h1>All Collections</h1>
      <div className="row">
        {filteredCollections.map(collection => (
          <div key={collection._id} className="col-md-4 mb-4">
            <CollectionCard collection={collection} />
          </div>
        ))}
      </div>
    </div>
  );
}

