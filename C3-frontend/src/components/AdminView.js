import {Table} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import EditCollection from './EditCollection.js';
import ArchiveCollection from './ArchiveCollection.js';

export default function AdminView({ collectionsData, fetchCollections }) {
  const [collections, setCollections] = useState([]);

  useEffect(() => {
    const collectionsArray = collectionsData.map((collection) => {
      return (
        <tr key={collection._id}>
          <td>{collection._id}</td>
          <td>{collection.category}</td>
          <td>{collection.name}</td>
          <td>{collection.description}</td>
          <td>{collection.price}</td>
          <td>{collection.isActive ? 'Available' : 'Unavailable'}</td>
          <td>
            <img
              src={`${process.env.REACT_APP_API_URL}/uploads/${collection.imageUrl}`}
              alt={collection.name}
              style={{ maxWidth: '100px', maxHeight: '100px' }}
            />
          </td>
          <td>
            <EditCollection
              collection_id={collection._id}
              fetchCollections={fetchCollections}
            />
          </td>
          <td>
            <ArchiveCollection
              collection_id={collection._id}
              fetchCollections={fetchCollections}
              isActive={collection.isActive}
            />
          </td>
        </tr>
      );
    });
    setCollections(collectionsArray);
  }, [collectionsData, fetchCollections]);

  return (
    <>
      <h1>Admin Dashboard</h1>

      <Table striped bordered hover responsive>
        <thead>
          <tr>
            <th>ID</th>
            <th>Category</th>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Availability</th>
            <th>Image</th>
            <th colSpan={2} className="text-center">
              Actions
            </th>
          </tr>
        </thead>
        <tbody>{collections}</tbody>
      </Table>
    </>
  );
}
