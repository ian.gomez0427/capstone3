
import React, { useState, useEffect } from 'react';
import CollectionCard from './CollectionCard.js';
import { Container, Row, Col, Form } from 'react-bootstrap'; // Assuming you want to add a category filter using a Form component

export default function UserView({ collectionsData }) {
  const [activeCollections, setActiveCollections] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState(''); // State to keep track of the selected category

  useEffect(() => {
    const activeCollectionsArray = collectionsData.filter(
      (collection) => collection.isActive === true
    );

    if (selectedCategory) {
      const filteredCollections = activeCollectionsArray.filter(
        (collection) => collection.category === selectedCategory
      );
      setActiveCollections(filteredCollections);
    } else {
      setActiveCollections(activeCollectionsArray);
    }
  }, [collectionsData, selectedCategory]);

  const handleCategoryChange = (event) => {
    setSelectedCategory(event.target.value);
  };

  return (
    <Container className = "my-5">
      <Form.Group controlId="categoryFilter">
        <Form.Label>Filter by Category:</Form.Label>
        <Form.Control as="select" value={selectedCategory} onChange={handleCategoryChange}>
          <option value="">All Categories</option>
          <option value="Poster">Poster</option>
          <option value="Figure">Figure</option>
          <option value="Manga">Manga</option>
        </Form.Control>
      </Form.Group>
      <Row xs={1} md={2} lg={4} className="my-3 g-4">
        {activeCollections.map((collection) => (
          <Col key={collection._id}>
            <div> 
              <CollectionCard collection={collection} />
            </div>
          </Col>
        ))}
      </Row>
    </Container>
  );
}

