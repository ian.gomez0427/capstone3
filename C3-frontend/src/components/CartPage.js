import React, { useContext, useState } from 'react';
import { Container, Row, Col, Card, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import { useCart } from '../hooks/useCart';

export default function CartPage() {
  const { user } = useContext(UserContext);
  const { cartItems, fetchCartItems, updateCartItem } = useCart();
  
  const handleQuantityChange = (itemId, newQuantity) => {
    updateCartItem(itemId, newQuantity); // Update the quantity in the cart
  };

  const handleCheckout = () => {
    // Implement your checkout logic here
    // You can navigate to a checkout page, process payments, etc.
  };

  return (
    <Container className="my-5">
      <h2>Your Cart</h2>
      <Row>
        {cartItems.map(item => (
          <Col key={item._id} md={3} className="mb-4">
            <Card classname = "p-5">
              <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '400px' }}>
                <Card.Img
                  variant="top"
                  src={`${process.env.REACT_APP_API_URL}/uploads/${item.collectionId.imageUrl}`}
                  style={{ maxWidth: '100%', maxHeight: 'auto', width: 'auto', height: '100%' }}
                />
              </div>
              <Card.Body>
                <Card.Title>{item.collectionId.name}</Card.Title>
                <Card.Text>
                  Quantity: 
                  <input
                    type="number"
                    value={item.quantity}
                    onChange={e => handleQuantityChange(item._id, parseInt(e.target.value))}
                  />
                </Card.Text>
                <Card.Text>Subtotal: ${item.subtotal}</Card.Text>
              </Card.Body>
            </Card>
          </Col>
        ))}
      </Row>
      <Button variant="primary" onClick={() => handleCheckout()}>
        Checkout
      </Button>
    </Container>
  );
}

